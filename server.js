const express = require('express')
const path = require('path')

const app = express()
const server = require('http').createServer(app)

const io = require('socket.io')(server)

app.use(express.static(path.join(__dirname, 'public')))
app.set('views', path.join(__dirname, 'public'))
app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')

app.use('/', (req, res) => {
    res.render('index.html')
})

let colMessages = []

io.on('connection', socket => {
    console.log('Socket conectado: ' + socket.id)

    socket.emit('previousMessage', colMessages)

    socket.on('sendMessage', function(data) {
        colMessages.push(data)

        socket.broadcast.emit('receiveMessage', data)
    })
})

server.listen(3000)